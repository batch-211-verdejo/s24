console.log("Ni Hao!");

const getCube = (x) => x ** 3;

console.log(getCube(3));

// Template Literals

let message = `The cube of ${3} is ${getCube(3)}.`;
console.log(message);

// Array Destructuring
const address = ["Finance St.", "SpringVille", "Almanza Uno", "Las Pinas", "Las Pinas", "NCR", "Philippines"];

const [streetName, subdivision, barangay, city, province, region, country] = address;

console.log(`My address is ${streetName}, ${subdivision}, ${barangay}, ${city}, ${country}`);

// Object Destructuring
const animal = {
    name: "Philippine conyo",
    scientificName: "Tsongus paredudus",
    alergen: "fake designer brands",
    slang: "taglish"

};

const {name, scientificName, alergen, slang} = animal;

message = `${name} is a native of Philippine ecosystem. It has a scientific name of ${scientificName}. Typically allergic to ${alergen} and uses ${slang} dialect.`;

console.log(message);

// Arrow Functions
const numArray = [1, 2, 3, 4, 5];

numArray.forEach((number) => {
    console.log(number);
});

/* const reduceNumber = (number1, number2, number3, number4, number5) => {
    console.log(number1 + number2 + number3 + number4 + number5);
};

reduceNumber(1, 2, 3, 4, 5); */

const reduceNumber = numArray.reduce((x, y) => x + y);

console.log(reduceNumber);

class Dog {
    constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;

    }
};

const dog1 = new Dog("Pocket", "3 months", "teacup");
console.log(dog1);

